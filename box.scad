play = 0.2;
epsilon=0.0001; // very small distance used to make sure compontents overlap where needed.

// how round should things be
$fn=18;  // for development
//$fn=50;  // for printing

boardWidth=27 + 2 * play;
boardLength=122 + 2 * play;
topInsideHeight=10;

segmentWidthOffset = 3.5 + 2 * play;
segmentLengthOffset = 7.5 + 2 * play;
segmentHeight = 8;
boardHeight = topInsideHeight-segmentHeight;

bottomHeight=20;
holeDiameter=5+2*play;

wallThickness = 2;
roundingRadius = 3;

outershellOffsetToBoard=wallThickness/2; //measured at 0.71 but round it up for convenience

topWidth=boardWidth + 2*outershellOffsetToBoard + 2*wallThickness;
topLength=boardLength + 2*outershellOffsetToBoard + 2*wallThickness;
topHeight=segmentHeight;

//=== top part ===//
translate([0,0,wallThickness+boardHeight+bottomHeight+wallThickness]) // for printing
union(){
    difference() {        
        // very top part  
        boxTop(topWidth, topLength, segmentHeight-wallThickness);
        
        // very top part inside
        translate([wallThickness,wallThickness,0]) 
            boxTop(topWidth-2*wallThickness,
                topLength-2*wallThickness,
                topHeight-wallThickness-wallThickness
        );
        
        // 7segment cutout        
        translate([wallThickness+outershellOffsetToBoard+segmentWidthOffset, 
            wallThickness+outershellOffsetToBoard+segmentLengthOffset, 
            topHeight-3*wallThickness-1*epsilon]) 
            cube([boardWidth-2*segmentWidthOffset,
                boardLength-2*segmentLengthOffset,2*wallThickness+2*epsilon]);        
    }
    
    // board support section
    translate([0,0,-wallThickness])
    squareTube(topWidth, topLength, wallThickness, 2*wallThickness);

    // board section
    translate([0,0,-wallThickness-boardHeight])
    squareTube(topWidth, topLength, boardHeight, wallThickness+outershellOffsetToBoard);
    
    // inner section
    translate([0,0,-wallThickness-boardHeight-bottomHeight-wallThickness])
    difference() {        
        squareTube(topWidth, topLength, bottomHeight + wallThickness, wallThickness);    
            
         // wire hole    
        translate([topWidth/2,wallThickness*2,
            holeDiameter/2+3*wallThickness]) 
        rotate([90,0,0])cylinder(h=100,d=holeDiameter);
    }
    
    // bottom stopper
    translate([0,0,-wallThickness-boardHeight-bottomHeight+wallThickness])
    squareTube(topWidth, topLength, boardHeight, wallThickness+outershellOffsetToBoard);   
}


//=== bottom lid ===//
lidWallOffset=2*(wallThickness+play);

translate([-2*wallThickness,lidWallOffset/2,2*wallThickness]) rotate([0,180,0])  // for printing
//translate([lidWallOffset/2,lidWallOffset/2,-5]) // visual fit
union(){
    // ring
    translate([0,0,wallThickness])
        squareTube(topWidth-lidWallOffset, topLength-lidWallOffset, boardHeight, wallThickness+outershellOffsetToBoard);
    // very bottom
    boxMiddle(topWidth-lidWallOffset,topLength-lidWallOffset,wallThickness);
}



//=== modules ===//

module boxTop(width,length,height) {    
    //%cube([width,length,height]);
    
    difference() {
        translate([roundingRadius,roundingRadius,0])     
            minkowski(){
                cube([width-2*roundingRadius,length-2*roundingRadius, 
                    height-roundingRadius]); // core
                sphere(r=roundingRadius); // rounding
            }
        
        // cut away the bottom part so it has a flat bottom
        translate([-epsilon,-epsilon,-roundingRadius+epsilon]) 
                cube([width+2*epsilon,length+2*epsilon,
                    roundingRadius]);
    }
    
}

module boxMiddle(width,length,height) {    
    //%cube([width,length,height]);
    translate([roundingRadius,roundingRadius,0]) 
    minkowski(){
        cube([width-2*roundingRadius,length-2*roundingRadius,height]);  // core
        cylinder(h=epsilon,r=roundingRadius); // rounding
    }
}

module squareTube(width,length,height, wallThickness) {    
     difference() {
        // outer part
        boxMiddle(width,length,height);
    
        // inner part
        translate([wallThickness,wallThickness,-epsilon]) 
            boxMiddle(width-2*wallThickness,length-2*wallThickness,height+2*epsilon);
     }
}
